package AutomationCases;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ConsumerMernisVerify {

	WebDriver driver;
	
	@BeforeTest
	public void SetUp(){
		
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		driver = new InternetExplorerDriver();
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");	
	}
	@Test(priority=1, description="Login functionality")
	public void LogintoCrm(){
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("wiprobaslan");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda123456789**");
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	@Test(priority=2)
	public void YeniSorgulama(){
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='M��teri-Abone Listesi']")).click();
	}
	@Test(priority=3)
	public void InquiryCustomer(){
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys("5343352112");
		driver.findElement(By.xpath("//input[@name='bul']")).sendKeys(Keys.ENTER);
	}
	@Test(priority=4)
	public void SairIslem() throws InterruptedException{
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@value='Sair i�lem']")).sendKeys(Keys.ENTER);
	}
	@Test(priority=5)
	public void MernisLink() throws InterruptedException{
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.cssSelector("//a[href*='396']")).sendKeys(Keys.ENTER);
	}
	@Test(priority=6)
	public void TcKimlikNo() throws InterruptedException{
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		Select selectObject = new Select (driver.findElement(By.xpath("//select[@name='treason']")));
		selectObject.selectByValue("AB.ISTEGI");
		driver.findElement(By.xpath("//input[@name='description']")).sendKeys("mernis");
		driver.findElement(By.xpath("//input[@name='T.C. Kimlik No Sorgulama']")).sendKeys(Keys.ENTER);
	}
	@Test(priority=7)
	public void VerificationComment() throws InterruptedException{
	Thread.sleep(2000);
	String a = driver.findElement(By.xpath("//b[contains(text(),'Islem Basarili Olmustur.')]")).getText();
	System.out.println(a);
	}
	@AfterClass
	public void quitdriver(){
		driver.quit();
	}
}
