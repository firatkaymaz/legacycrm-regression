package AutomationCases;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ConsumerGsmNoChange {

	WebDriver driver;
	
	@BeforeTest
	public void SetUp(){
		
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		driver = new InternetExplorerDriver();
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");	
	}
	@Test(priority=1, description="Login functionality")
	public void LogintoCrm(){
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("wiproasirin");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda353535**");
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	
	@Test(priority=2, description="Click Sorgulama")
     public void Inquiry(){
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu").findElement(By.xpath("//a[@title='M��teri-Abone Listesi']")).click();
	}
	
	@Test(priority=3, description="Search Number")
    public void SearchNumber() throws InterruptedException{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys("5467021063");
		driver.findElement(By.xpath("//input[@name='bul']")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@value='Sair i�lem']")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("//a[href*='301']")).click(); 
	}
	
	
}
