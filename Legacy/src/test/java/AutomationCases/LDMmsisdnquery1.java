package AutomationCases;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LDMmsisdnquery1 {
	
WebDriver driver;
	
	@BeforeTest
	public void SetUp(){
		
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		driver = new InternetExplorerDriver();
		driver.get("http://10.86.129.80:9091/LDM/Menu/showLogin.html");	
	}
	@Test(priority=1, description="Login functionality")
	public void LogintoLDM(){
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("wiproasirin");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda353535**");
		WebElement login=driver.findElement(By.xpath("//input[@src='image3.gif']"));
		login.click();
	}
	@Test(priority=2, description="Click CDA menu")
	
	public void ClickCDA(){
		
		driver.switchTo().frame("bx");
		driver.findElement(By.xpath("//input[@id='appsect' and @value='CDA']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='GSM Sorgula']")).click();
	}
	
	@Test(priority=3, description="Inquiry Msisdn")
	
	public void InquiryMsisdn() throws ClassNotFoundException, SQLException, InterruptedException{
		
		String msisdn = null;
		String value ;
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		//driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		value=DBConnection2(msisdn);
		driver.findElement(By.xpath("//input[@name='msisdn']")).sendKeys(value);
		driver.findElement(By.xpath("//input[@name='msisdn1']")).sendKeys(value);
	}
	
	public static String DBConnection2(String icci) throws ClassNotFoundException, SQLException{
		
		 Class.forName("oracle.jdbc.OracleDriver");
		   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		   Statement stmt = conn.createStatement();
		   stmt.executeQuery("SELECT * FROM ccb.ccb_subscriber WHERE status='A' and customer_id IN (SELECT customer_id FROM ccb.ccb_customer WHERE TYPE = 'S' and tax_number is NULL) AND (gsm_no, start_date) IN (SELECT gsm_no, start_date FROM ccb.ccb_subscriber_package sp WHERE package_id like ('PP%') AND sw_date IN (SELECT MAX (sw_date) FROM ccb.ccb_subscriber_package WHERE gsm_no = sp.gsm_No AND start_date = sp.start_Date) and gsm_no not in (SELECT gsm_no FROM ccb.ccb_provision_log where gsm_no=sp.gsm_no and TRANSACTION_CODE='500' and log_status='E')) and start_date<sysdate-10 order by cr_date desc");
		   ResultSet rs = stmt.getResultSet();
		   if(rs.next()){
			  String s  = rs.getString(1);
			  icci = s;
		   }
		return icci;
	}
	

}
