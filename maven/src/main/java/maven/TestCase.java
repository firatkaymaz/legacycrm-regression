package maven;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;



public class TestCase {

	public static void main(String[] args) throws AWTException, InterruptedException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		WebDriver driver = new InternetExplorerDriver();	
		//LineActivation.SecurityPage(driver);
		LoginPage (driver);
		SimCardChange (driver);
		SimCardChange2 (driver);

	}
	
	public static void LoginPage (WebDriver driver){
		
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("wiprobaslan");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda123456789**");
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	
	public static void SimCardChange (WebDriver driver) throws AWTException, InterruptedException{
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='M��teri-Abone Listesi']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys("5467028283");
		driver.findElement(By.xpath("//input[@name='bul']")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@value='Sair i�lem']")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.cssSelector("//a[href*='301']")).click(); 
		Thread.sleep(1000);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}
	
	public static void SimCardChange2 (WebDriver driver) throws ClassNotFoundException, SQLException{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		String mainWindoww = driver.getWindowHandle();
		System.out.println(mainWindoww);
		driver.findElement(By.xpath("//input[@name='simcardICCI']")).sendKeys(Keys.ENTER);
		//driver.findElement(By.xpath("//input[@name='simcardICCI']")).click();
		//Thread.sleep(1000);
		Windows(driver, mainWindoww);
		String simcardicci = null;
		driver.findElement(By.xpath("//input[@name='icci']")).clear();
		driver.findElement(By.xpath("//input[@name='icci']")).sendKeys(DBConnection2(simcardicci));
		driver.findElement(By.xpath("//input[@value='BUL']")).click();
		//Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@type='button']")).click();
		//Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@class='button']")).sendKeys(Keys.ENTER);
		//WebElement mybutton = driver.findElement(By.xpath("//input[@class='button']"));
		//Point point = mybutton.getLocation();
		//int xdirection = driver.findElement(By.xpath("//input[@class='button']")).getLocation().getX();
		//int ydirection = driver.findElement(By.xpath("//input[@class='button']")).getLocation().getY();
		//click (xdirection,ydirection);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.switchTo().window(mainWindoww);
		//Thread.sleep(3000);
	    driver.switchTo().defaultContent();
	    driver.switchTo().frame("cx");
	    driver.findElement(By.xpath("//input[@name='degistir']")).click();
	    //Thread.sleep(1000);
	}
	public static String DBConnection2(String icci) throws ClassNotFoundException, SQLException{
		
		 Class.forName("oracle.jdbc.OracleDriver");
		   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		   Statement stmt = conn.createStatement();
		   stmt.executeQuery("select * from ccb.ccb_icci_sim_pool WHERE USAGE_REASON_CODE = 'YD' AND icci IN (SELECT icci FROM ccb.ccb_imsi_pool WHERE gsm_no IS NULL and brand_code='MC' and imsi_status_code='PS') AND CARD_TYPE = 'NO' AND ICCI_STATUS_CODE = 'BA' AND LOCK_IND = 'H' AND CAMPAIGN_CODE='DEF_MC'");
		   ResultSet rs = stmt.getResultSet();
		   if(rs.next()){
			  String s  = rs.getString(1);
			  icci = s;
		   }
		return icci;
	}
	public static void Windows (WebDriver driver, String mainWindow) {
		
		mainWindow = driver.getWindowHandle();
		Set<String> totalwindowsIds = driver.getWindowHandles();
		int a=0;
		String[] myset = totalwindowsIds.toArray(new String[totalwindowsIds.size()]);	
			if(myset[a].contains(mainWindow)){
				driver.switchTo().window(myset[a+1]);
			}
			else{
				driver.switchTo().window(myset[a]);
			}
	}

}
