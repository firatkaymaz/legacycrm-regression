Feature: Create Customer
  Scenario: Create Prepaid Customer Turkish Citizen
    Given access to legacyCRM
    When type your username and password
    And click login button on the screen
    Then verify that legacyCrm is opened
    And click Create Customer
    Then type concerned customer information
    And continue to populate customer informations
    And type customer's detailed informations
    And type customer's address informations
    When type customer's payment informations
    Then verify the customer number is created
    