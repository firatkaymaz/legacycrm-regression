package Auto;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BiometricOrders {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
		// TODO Auto-generated method stub
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		WebDriver driver = new InternetExplorerDriver();	
		//LineActivation.SecurityPage(driver);
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
		LineActivation.LoginPage(driver);
		BiometricOrderScreen(driver);
		DBConnectionVerify();
		BiometricOrderScreen2(driver);
		DBConnectionVerify2();
		
	}
	
	public static void BiometricOrderScreen (WebDriver driver) throws InterruptedException {
		
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='biometric Check List']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys("5467002707");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@value='Sorgula']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@value='Tablete G�nder']")).click();
		Thread.sleep(3000);	
	}
	public static void DBConnectionVerify() throws ClassNotFoundException, SQLException{
		
		 Class.forName("oracle.jdbc.OracleDriver");
		   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		   System.out.println("DB Connection is ok");
		   Statement stmt = conn.createStatement();
		   ResultSet rs = stmt.executeQuery("select * from ccb.ccb_bio_queue where gsm_no='5467002383'");
		  //rs.findColumn("STATUS");
		  while(rs.next()){
			String column = rs.getString(13);
			System.out.println(column);
		  if(column.matches("Initial")){
			  System.out.println("It's ready for sending to Tablet");
		  }
			  
		  }
	}
	
	public static void BiometricOrderScreen2 (WebDriver driver) throws InterruptedException {
		
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys("5467029833");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@value='Sorgula']")).click();
		Thread.sleep(1000);
		List<WebElement> elements = driver.findElements(By.xpath("//td[@height]"));
		System.out.println(elements.get(4).getTagName().toString());
		driver.findElement(By.xpath("//input[@value='Tablete G�nder']")).click();
		Thread.sleep(1000);	
		driver.findElement(By.xpath("//input[@value='Manuel Evrak']")).click();
		System.out.println(elements.get(4).getTagName().toString());
	}
	public static void DBConnectionVerify2() throws ClassNotFoundException, SQLException{
		
		 Class.forName("oracle.jdbc.OracleDriver");
		   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		   System.out.println("DB Connection is ok");
		   Statement stmt = conn.createStatement();
		   ResultSet rs = stmt.executeQuery("select * from ccb.ccb_bio_queue where gsm_no='5461113410'");
		  //rs.findColumn("STATUS");
		  while(rs.next()){
			String column = rs.getString(13);
			System.out.println(column);
		  if(column.matches("CANCELLED")){
			  System.out.println("It's cancelled");
		  }
			  
		  }
	}
}
	